require 'gem2deb/rake/testtask'

Rake::TestTask.new(:test) do |t|
  t.libs << "test"
  t.test_files = FileList['test/all.rb']
  t.verbose = true
end

task :default => :test
